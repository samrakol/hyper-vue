export const textMixin = {
   computed:{
         reversedTextMixin() {
            return  [...this.text].reverse().join('');
         },
         textSizerMixin() {
            return `${this.text} (${this.text.length} charactes long)`;
         }
   }
}
