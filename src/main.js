import Vue from 'vue'
import App from './App.vue'
import Header from './layout/Header.vue'
import Footer from './layout/Footer.vue'
import setupVueResources from './services/setupVueResources.js'
import setupAxios from './services/setupAxios.js'
import {router} from './router/router'
import {store} from './store/store'


Vue.component('vue-ex-header',Header)
Vue.component('vue-ex-footer',Footer)

setupAxios();
setupVueResources();

export const eventBus = new Vue({
})

new Vue({
  el: '#app',
  router: router,
  store:store,
  render: h => h(App)
})
