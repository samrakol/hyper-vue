const state={
   counter: 0
}

const getters={
   counterX2: state => {
      return state.counter*2
   },
   counterHEX: state=>{
      console.log('initial = '+state.counter);
      return "Number of clickcs = #" + state.counter.toString(16)
   },
}

const mutations={
   increment: state => {
      console.log('before increment = '+state.counter);
      state.counter++;
      console.log('after increment = '+state.counter);
   },
   decrement: state => {
      state.counter--;
   },
   incrementBy: (state,n) => {
      state.counter+=n;
   },
   decrementBy: (state,n) => {
      state.counter-=n;
   }, 
}

const actions={
   incBy: (context, payload) =>{
      context.commit('incrementBy',payload)
   }, 
   decBy: ({commit},payload) => { //using ES6 object destrucururing
      commit('decrementBy',payload)
   },
   asyncIncBy: ({commit},payload) =>{
      setTimeout(()=>{
         commit('incrementBy',payload.by);
      },payload.delay)
      
   }, 
   asyncDecBy: ({commit},payload) => { 
      setTimeout(()=>{
         commit('decrementBy',payload.by);
      },payload.delay)
   },
}

export default {
   state,
   getters, //using ES6 syntax
   mutations, 
   actions
}