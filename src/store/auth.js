import axios from 'axios'
import auth from '../services/axiosAuth.js'
import {router} from '../router/router'

const API_KEY = '?key=AIzaSyDGu5xI05vEI3h2Mr_Hzt7z0R1pUCrXfD8';

const refresh = axios.create({
   baseURL:'https://securetoken.googleapis.com/v1/'
})
const state={
   idToken:null,
   userID:null, 
   refreshToken:null,
   exparation:null,
   users:[]
}

const getters={
   getUsers : (state) => {
      return state.users;
   },
   idToken : (state) => {
      return state.idToken
   },
   isAuthenticated: (state => {
      return state.idToken !== null
   })
}
function localStore(){
   localStorage.setItem('idToken',state.idToken);
   localStorage.setItem('userID',state.userID);
   localStorage.setItem('refreshToken',state.refreshToken);
   localStorage.setItem('exparation',state.exparation);
}
const mutations={
   authUser: (state,userData) => {
      state.idToken = userData.token;
      state.userID = userData.userID;
      state.refreshToken = userData.refreshToken;
      state.exparation = userData.exparation;
   },
   updateUsers: (state,users) => {
      state.users = users
   },
   signout: (state) =>{
      state.idToken = null;
      state.userID = null;
      state.refreshToken = null;
      state.exparation = null;
   }
}

const actions={
   register: (context, payload) =>{
      const authData = {
         email:payload.email,
         password:payload.password,
         returnSecureToken:true
      }
      auth.post(`/signupNewUser?${API_KEY}`,authData)
      .then(resp=>{
         console.log(resp)
         context.commit('authUser',{
            token:resp.data.idToken,
            userID:resp.data.localId,
            refreshToken:resp.data.refreshToken,
            exparation:(new Date()).getTime() + resp.data.expiresIn*1000
         })
         localStore();
         context.dispatch('storeUser',payload);
         context.dispatch('refreshToken',resp.data.expiresIn)   
      })
      .catch(error => console.log(error)) 
   }, 
   signin: (context,payload) => { 
      const authData = {
         email:payload.email,
         password:payload.password,
         returnSecureToken:true
      }
      auth.post(`/verifyPassword${API_KEY}`,authData)
      .then(resp=>{
         console.log(resp)
         context.commit('authUser',{
            token:resp.data.idToken,
            userID:resp.data.localId,
            refreshToken:resp.data.refreshToken,
            exparation:(new Date()).getTime() + resp.data.expiresIn*1000
         })
         localStore(resp.data.expiresIn);
         context.dispatch('refreshToken',resp.data.expiresIn)   
      })
      .catch(error => console.log(error.response))
   },
   attemptAutoSignin: (context) =>{
      const authUser = {}
      authUser.token = localStorage.getItem('idToken');
      if (authUser.token) {
         authUser.exparation = localStorage.getItem('exparation');
         const timeleft = authUser.exparation - (new Date()).getTime();
         if (timeleft>0){
           authUser.refreshToken = localStorage.getItem('refreshToken');
           authUser.userID = localStorage.getItem('userID');
           context.commit('authUser',authUser)
           context.dispatch('refreshToken',timeleft/1000)   
         }
      }

   },
   signout: ({commit}) =>{
      commit('signout');
      router.replace('/axios');
      localStore(0);
   },
   refreshToken: (context,payload) =>{
      console.log('Setup refresh afer:'+payload)
      setTimeout(()=>{
         if (context.state.refreshToken){
            refresh.post(`token${API_KEY}`,{
               grant_type:"refresh_token",
               refresh_token:context.state.refreshToken
            }).then(resp=>{
               console.log('Token refresh success')
               context.commit('authUser',{
                  token:resp.data.id_token,
                  userID:resp.data.user_id,
                  refreshToken:resp.data.refresh_token,
                  exparation:(new Date()).getTime + resp.data.expiresIn*1000
               }) 
               context.dispatch('refreshToken',resp.data.expires_in)   
            }).catch(error=>{
               console.log(error)
               context.dispatch('signout');
         })}
      },(payload-1)*1000)
   },
   storeUser : ({commit,state},payload) => { 
      if (state.idToken){
         axios.post('/users.json?auth='+state.idToken,payload)
         .then(resp=>console.log(resp))
         .catch(error => console.log(error))
      }
   },
   getUsers: ({commit,state}) => { 
      axios.get('/users.json?auth='+state.idToken)
      .then(resp => {
        const users = Object.keys(resp.data).reduce((array, k) => {      
        return array.concat({id:k,...resp.data[k]});
        }, []);
        commit('updateUsers',users)
      })
      .catch(error => console.log(error));

   }
}

export default {
   state,
   getters, 
   mutations, 
   actions
}