import Vue from 'vue';
import Vuex from 'vuex';
import counter from './modules/counter';
import value from './modules/value';
import auth from './auth';

import * as getters from './getters.js'
import * as mutations from './mutations.js'
import * as actions from './actions.js'

Vue.use(Vuex);


export const store = new Vuex.Store({
   state:{
      //store all shared properties which are not in modules
      title:'Vuex',
   },
   getters,
   mutations,
   actions,
   modules:{
      counter,
      value,
      auth
   }
});
